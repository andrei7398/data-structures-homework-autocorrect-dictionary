//SFETCU IULIAN-ANDREI 312CD

#ifndef TABLE_H
#define TABLE_H

#include <stdio.h>
#include <stdlib.h>

// List element for Dictionary lists.
typedef struct ListNode {
	struct ListNode *next;
	struct ListNode *prev;
	char *key;
	char *value;
	int frequency;
} ListNode;

// Dictionary structure that includes the lists of elements and their number.
typedef struct Dictionary {
	ListNode **lists;		// lists of elements.
	int N;					// number of lists.
} Dictionary;


// Initializes an empty Dictionary structure.
Dictionary* createDictionary(int N);

// Adds an element to the Dictionary structure.
void addElement(Dictionary *dictionary, char *key, char *value, int frequency);

// Removes an element from the Dictionary structure.
void removeElement(Dictionary *dictionary, char *key, char *value);

// Prints all the elements from all the lists of the Dictionary structure.
void printDictionary(FILE *f, Dictionary *dictionary);

// Gets all the elements with the specified key and increments the frequencies.
ListNode* get(Dictionary *dictionary, char *key);

// Prints all the elements with the specified value.
void printValue(FILE *f, Dictionary *dictionary , char *value);

// Prints all the elements with the specified frequency.
void printFrequency(FILE *f, Dictionary *dictionary , int frequency);

// Returns a list containing the elements with the specified value.
ListNode* unionValues(Dictionary *dictionary, char *value);

// Returns a list containing the elements with maximum frequency in each list.
ListNode* unionMaxFrequencies(Dictionary *dictionary);

// Returns a new Dictionary with reversed lists of the input structure.
Dictionary* reverseLists(Dictionary *dictionary);

// Prints a double-linked non-circular list.
void printList(FILE *f, ListNode *list);

// Frees all the memory allocated for the Dictionary.
void freeDictionary(Dictionary *dictionary);

// Frees all the memory allocated for a double-linked non-circular list.
void freeList(ListNode *list);

//------------------------------------------------------------------------------

Dictionary* createDictionary(int N) {

	Dictionary *dictionary = (Dictionary*)malloc(sizeof(Dictionary));

// Checking if was enough memory

	if (dictionary == NULL)
		return NULL;

	dictionary->N = N;

	dictionary->lists = (ListNode**)calloc(N,sizeof(ListNode*));

	dictionary->N = N;

	return dictionary;

	return NULL;
}

// Function that calculates the element list number (r)

int calculateR(char *key, int N) {

	int i, r = 0, sum = 0;

    	for (i = 0; i < strlen(key); i++) {
    	    sum = sum + key[i];
    	}

    	r = sum % N;

    	return r; 
}

// Function that counts the number of elements in a list

int countlis(ListNode *list) {

	ListNode *curr = list;
	int countl = 0;

	if (list == NULL)
		return countl;

	do {
		countl++;
		curr = curr->next;
	} while (curr->next != list);

	return countl;
}

// Function that counts the number of elements in the whole dictionary

int countdic(Dictionary *dictionary) {

	int i, count = 0;

	for(i = 0; i < dictionary->N; i++) {

		ListNode *first = dictionary->lists[i];
		ListNode *curr = dictionary->lists[i];

		if (!curr) 
			continue;
		do {
			count++;
			curr = curr->next;
		} while (curr->next != first);
	}

	return count;
}


void arrangekey(ListNode *left, ListNode *right, ListNode *curr, ListNode *newNode) {

	if(curr->next == right) {
		
		while (curr->prev != left) {
			if (strcmp(curr->key,newNode->key) < 0) {
				newNode->prev = curr;
				newNode->next = curr->next;
				curr->next->prev = newNode;
				curr->next = newNode;
				break;
			}
			if (strcmp(curr->key,newNode->key) == 0)
				curr->frequency += newNode->frequency;
			curr = curr->prev;
		}
	}
	else if(curr->prev == left) {

		while (curr->next != right) {
			if (strcmp(curr->key,right->key) > 0) {
				newNode->next = curr;
				newNode->prev = curr->prev;
				curr->prev->next = newNode;
				curr->prev = newNode;
				break;
			}
			if (strcmp(curr->key,newNode->key) == 0)
				curr->frequency += newNode->frequency;
			curr = curr->next;
		}
	}
	return;
}

void arrangevalue(ListNode *left, ListNode *right, ListNode *curr, ListNode *newNode) {

	if(curr->next == right) {

		while (curr->prev != left) {
			if (strcmp(curr->value,newNode->value) < 0) {
				newNode->prev = curr;
				newNode->next = curr->next;
				curr->next->prev = newNode;
				curr->next = newNode;
				break;
			}
			if (strcmp(curr->value,newNode->value) == 0) {
				curr = curr->prev;
			}
			if (strcmp(curr->next->value,newNode->value) == 0 && curr->next != right) {
				left = curr;
				curr = curr->next;
				arrangekey(left,right,curr,newNode);
				break;
			}
			right = curr;
			curr = curr->prev;
		}
	}
	else if(curr->prev == left) {

		while (curr->next != right) {
			if (strcmp(curr->value,right->value) > 0) {
				newNode->next = curr;
				newNode->prev = curr->prev;
				curr->prev->next = newNode;
				curr->prev = newNode;
				break;
			}
			if (strcmp(curr->value,newNode->value) == 0) {
				curr = curr->next;
				continue;
			}
			if (strcmp(curr->prev->value,newNode->value) == 0 && curr->prev != left) {
				right = curr;
				curr = curr->prev;
				arrangekey(left,right,curr,newNode);
				break;
			}
			left = curr;
			curr = curr->next;
		}
	}
	return;
}

void addElement(Dictionary *dictionary, char *key, char *value, int frequency) {
	
// Checking if dictionary is NULL or empty

	if (dictionary == NULL)
		return;

// Creating the new node and saving the data inside him 

	ListNode *newNode = malloc(sizeof(ListNode));

	newNode->key = key;
	newNode->value = value;
	newNode->frequency = frequency;

// Calculating r with a auxiliary function for finding the element specific list

	int r = calculateR(key,dictionary->N);

// Counting the number of list elements with a auxiliary function

	int countl = countlis(dictionary->lists[r]);
	
// Check list requirment (number of elements < number of lists)

	if(countl >= dictionary->N) {
		ListNode *del = dictionary->lists[r]->prev;
		ListNode *last = del->prev;
		last->next = dictionary->lists[r];
		dictionary->lists[r]->prev = last;
		free(del);
	}

	if (dictionary->lists[r] == NULL) {
		dictionary->lists[r] = newNode;
		newNode->next = newNode;
		newNode->prev = newNode;
	}

// Inserting the node descending by frequency,then alphabetic by value then key using auxiliar functions

	ListNode *right = dictionary->lists[r];
	ListNode *curr = dictionary->lists[r]->prev;
	ListNode *left = curr->prev;
	while (r) {

// Case where curr is the last item

		if (right == dictionary->lists[r]) {
			if (curr->frequency == newNode->frequency) {
				while (curr->frequency == left->frequency && left != right)
					left = left->prev;
				arrangevalue(left,right,curr,newNode);
				break;
			}
			if(curr->frequency > newNode->frequency) {
				curr->next = newNode;
				newNode->prev = curr;
				newNode->next = right;
				right->prev = newNode;
				break;
			}
		}

// Case where curr is the first item

		if (left == dictionary->lists[r]->prev) {
			if (curr->frequency == newNode->frequency) {
				while(curr->frequency == right->frequency && right != left)
					right = right->next;
				arrangevalue(left,right,curr,newNode);
				break;
			}
			if(curr->frequency < newNode->frequency) {
				curr->prev = newNode;
				newNode->next = curr;
				newNode->prev = left;
				left->next = newNode;
				break;
			}
		}

// General case

		if (curr->frequency >= newNode->frequency && right->frequency < newNode->frequency) {
			if (curr->frequency == newNode->frequency) {
				while (curr->frequency == left->frequency)
					left = left->prev;
				arrangevalue(left,right,curr,newNode);
				break;
			}
			if(curr->frequency > newNode->frequency) {
				curr->next = newNode;
				newNode->prev = curr;
				newNode->next = right;
				right->prev = newNode;
				break;
			}
			break;
		}

// Moving all pointers until we find the right place

		else {
			right = curr; 
			curr = left; 
			left = curr->prev;
		}
	}

// Counting the number of dictionary elements with a auxuliary function

	int count = countdic(dictionary);

// Checking dictionary requirment (number of total elements < 2 * number of lists)

	if (count >= 2*(dictionary->N)) {
		int i;
		for (i = 0; i < dictionary->N; i++) {
			ListNode *newdel = dictionary->lists[i]->prev;
			ListNode *newlast = newdel->prev;
			newlast->next = dictionary->lists[i];
			dictionary->lists[i]->prev = newlast;
			free(newdel);
		}
	}

    return;
}

void removeElement(Dictionary *dictionary, char *key, char *value) {

	if (dictionary == NULL)
		return;

	if (dictionary->N == 0)
		return;

  	ListNode *curr, *last;
 
// Visit each node, maintaining a pointer to the previous node we just visited

	int i;
  	for(i = 0; i < dictionary->N; i++) {
  		last = NULL;
  		curr = dictionary->lists[i];

		if (!curr) 
			continue;

		if (curr->next == curr) {
			dictionary->lists[i] = NULL;
			free(curr);
			return;
		}

  		do {
			if (strcmp(key,curr->key) == 0 && strcmp(value,curr->value) == 0) {  
      				if (last == NULL) {
					curr->next->prev = curr->prev;
					curr->prev->next = curr->next;
        				dictionary->lists[i] = curr->next;
					free(curr);
					return;
      				}
      				else {
        				last->next = curr->next;
					curr-> next->prev=last;
					free(curr);
					return;
      				}
    			}
    			last = curr;
    			curr = curr->next;
  		} while(curr != dictionary->lists[i]);
  	}
}

void printDictionary(FILE *f, Dictionary *dictionary) {

	if (dictionary == NULL)
		return;

	int i;

	for (i = 0; i < dictionary->N; i++) {

		fprintf(f, "List %d: ",i);
		if (dictionary->lists[i] == NULL) { 
			fprintf(f, "\n");
			continue;
		}
			
		else {
			ListNode *curr = dictionary->lists[i];
			do {
				fprintf(f, "(%s, %s, %d) ", curr->key, curr->value, curr->frequency);
				curr = curr->next;
			} while (curr != dictionary->lists[i]);
		}
		
		fprintf(f,"\n");
	}
	return;
}

void Split(ListNode* list,ListNode** front, ListNode** back) {

    ListNode *fast;
    ListNode *slow;
    slow = list;
    fast = list->next;
 
// Advance 'fast' two nodes and advance 'slow' one node
    while (fast != NULL) {
    	fast = fast->next;
    	if (fast != NULL) {
        	slow = slow->next;
        	fast = fast->next;
    	}
    }
 
// 'slow' is before the midpoint in the list, so split it in two at that point. 
    *front = list;
    *back = slow->next;
    slow->next = NULL;
}

ListNode* SortedMerge(ListNode* a, ListNode* b) {

	ListNode *sorted = NULL;
 
// Base cases 
	if (a == NULL)
   		return(b);
	else if (b==NULL)
    	return(a);
 
// Pick either a or b, and recur
	if (a->frequency >= b->frequency) {
    	sorted = a;
    	sorted->next = SortedMerge(a->next, b);
	}
	else {
    	sorted = b;
    	sorted->next = SortedMerge(a, b->next);
	}
	return(sorted);
}

void MergeSort(ListNode *list) {

	ListNode *aux = list;
	ListNode *a;
	ListNode *b;
 
// Base case (length 0 or 1)
	if ((aux == NULL) || (aux->next == NULL))
    		return;
 
// Split head into 'a' and 'b' sublists using a auxiliary function
	Split(aux, &a, &b); 
 
// Recursively sort the sublists
	MergeSort(a);
	MergeSort(b);
 
// answer = merge the two sorted lists together
	list = SortedMerge(a, b);
}

ListNode* get(Dictionary *dictionary, char *key) {

	if (dictionary == NULL)
		return NULL;

	if (dictionary->N == 0)
		return NULL;

	int i;
	ListNode *aux;
	aux = NULL;

	for (i = 0; i < dictionary->N; i++) {
		ListNode *curr = dictionary->lists[i];
		do {
			if (strcmp(key,curr->key) == 0) {

				curr->frequency += 1;
				ListNode *newNode = malloc(sizeof(ListNode));
				newNode->key = curr->key;
				newNode->value = curr->value;
				newNode->frequency = curr->frequency;
				newNode->next = NULL;

				if (i == 0)
					newNode->prev = NULL;

				if (aux != NULL){
					newNode->prev = aux;
					aux->next = newNode;
				}
				aux = newNode;
			}
			curr = curr->next;	
		} while (curr->next != dictionary->lists[0]);
		MergeSort(curr);
	}

	if (aux != NULL)
		return aux;

	return NULL;
}

void printValue(FILE *f, Dictionary *dictionary , char *value) {

	if (dictionary == NULL)
		return;

	if (dictionary->N == 0)
		return;

	int i;

	for (i = 0; i < dictionary->N; i++) {
		ListNode *curr = dictionary->lists[i];
		do {
			if (strcmp(value,curr->value) == 0)
				fprintf(f, "(%s, %s, %d) ", curr->key, curr->value, curr->frequency);
			curr = curr->next;	
		} while (curr->next != NULL && curr->next != dictionary->lists[i]);
	}
	fprintf(f,"\n");
	return;
}

void printFrequency(FILE *f, Dictionary *dictionary , int frequency) {

	if (dictionary == NULL)
		return;

	if (dictionary->N == 0)
		return;

	int i;

	for (i = 0; i < dictionary->N; i++) {
		ListNode *curr = dictionary->lists[i];
		do {
			if (curr->frequency == frequency)
				fprintf(f, "(%s, %s, %d) ", curr->key, curr->value, curr->frequency);
			curr = curr->next;	
		} while (curr->next != NULL && curr->next != dictionary->lists[i]);
	}
	fprintf(f,"\n");
	return;
}

ListNode* unionValues(Dictionary *dictionary, char *value) {

	if (dictionary == NULL)
		return NULL;

	if (dictionary->N == 0)
		return NULL;

	int i;
	ListNode *aux;
	aux = NULL;

	for (i = 0; i < dictionary->N; i++) {
		ListNode *curr = dictionary->lists[i];
		do {
			if (strcmp(value,curr->value) == 0) {

				ListNode *newNode = malloc(sizeof(ListNode));
				newNode->key = curr->key;
				newNode->value = curr->value;
				newNode->frequency = curr->frequency;
				newNode->next = NULL;

				if (i == 0)
					newNode->prev = NULL;

				if (aux != NULL){
					newNode->prev = aux;
					aux->next = newNode;
				}
				aux = newNode;
			}
			curr = curr->next;	
		} while (curr->next != NULL && curr->next != dictionary->lists[i]);
	}
	return NULL;
}

ListNode* unionMaxFrequencies(Dictionary *dictionary) {

	if (dictionary == NULL)
		return NULL;

	if (dictionary->N == 0)
		return NULL;
	
	int i;
	ListNode *aux;
	aux = NULL;

	for (i = 0; i < dictionary->N; i++) {

		ListNode *newList = malloc(sizeof(ListNode));
		newList->key = dictionary->lists[i]->key;
		newList->value = dictionary->lists[i]->value;
		newList->frequency = dictionary->lists[i]->frequency;
		newList->next = NULL;

		if (i == 0)
			newList->prev = NULL;

		if (aux != NULL) {
			newList->prev = aux;
			aux->next = newList;
		}
		aux = newList;
	}
	return NULL;
}

Dictionary* reverseLists(Dictionary *dictionary) {

	if (dictionary == NULL)
		return NULL;

	Dictionary *newd = malloc(sizeof(Dictionary));

	newd->N = dictionary->N;
	if(newd->N != 0)
		newd->lists = malloc(sizeof(ListNode)*(newd->N));
	
// Copying the whole dictionary in a new one to reverse

	int i;
	for(i = 0; i < dictionary->N; i++) {

		ListNode *curr1 = dictionary->lists[i];
		ListNode *first = newd->lists[i];
		ListNode *last = NULL;

 		if(dictionary->lists[i] == NULL)
			continue;

		if(dictionary->lists[i]->next == dictionary->lists[i]) {

			ListNode *curr2 = malloc(sizeof(ListNode));
			newd->lists[i] = curr2;
			curr2->key = curr1->key;
			curr2->value = curr1->value;
			curr2->frequency = curr1->frequency;
			curr2->next = curr2;
			curr2->prev = curr2;
			curr1 = curr1->next;
			first = curr2;
			continue;
		 }
	
		do {
			ListNode *curr2 = malloc(sizeof(ListNode));
			curr2->key = curr1->key;
			curr2->value = curr1->value;
			curr2->frequency = curr1->frequency;
			curr2->next = first;
			if (last != NULL) {
				curr2->prev = last;
				last->next = curr2;
			}
			else {
				curr2->prev = curr2;
			}
			curr1 = curr1->next;			
			last = curr2;
		} while (curr1 != dictionary->lists[i]);
	}

// Reversing the new dictionary (newd)

	for (i = 0; i < newd->N; i++) {
		if (newd->lists[i] != NULL) {
			ListNode *aux = NULL;
    			ListNode *curr = newd->lists[i];
    			do {
        			aux = curr->prev;  
        			curr->prev = curr->next;   
        			curr->next = aux;
        			curr = curr->prev;
    			} while (curr != NULL && curr != newd->lists[i]);
    			newd->lists[i] = newd->lists[i]->prev;
		}
    	}

	return newd;

	return NULL;
}

void printList(FILE *f, ListNode *list) {

    ListNode *curr = list; 													

    if (list == NULL)
    	return;

    do { 																	
        fprintf(f, "%s %s %d ", curr->key, curr->value, curr->frequency); 	
        curr = curr->next; 													
    } while (curr != list);

	return; 
}

void freeDictionary(Dictionary *dictionary) {

	if (dictionary == NULL)
		return;

	if (dictionary->N == 0){
		free(dictionary);
		return;
	}

	int i;
	for (i = 0; i < dictionary->N; i++)
		freeList(dictionary->lists[i]);
	
	free(dictionary);

	return;
}

void freeList(ListNode *list) {

	if (list == NULL)
		return;
	
	if (list->next == list || list->prev == list) {
		free(list);
		return;
	}	

	ListNode *next = list->next;
	ListNode *first = list;

	while (next != first) {
   		free(list);
   		list = next;
		next = next->next;
	}

	return;
}

#endif



